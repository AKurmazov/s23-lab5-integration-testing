import json

import requests

BUDGET_MINUTE = 22
LUXURY_MINUTE = 56
FIXED_KM = 12
DEVIATIONS = 1.11
DISCOUNT = 0.93

TYPES = ('budget', 'luxury')
PLANS = ('fixed_price', 'minute')
DISTANCES = PLANNED_DISTANCES = TIMES = PLANNED_TIMES = (8, 16)
IS_DISCOUNTED = ('yes', 'no')


def calculate_ride(type, plan, distance, planned_distance, time, planned_time, is_discounted):
    if type == 'budget':
        if plan == 'fixed_price':
            if (distance / planned_distance > DEVIATIONS) or (time / planned_time > DEVIATIONS):
                cost = BUDGET_MINUTE * time
            else:
                cost = FIXED_KM * planned_distance
        elif plan == 'minute':
            cost = BUDGET_MINUTE * time
    elif type == 'luxury':
        if plan == 'fixed_price':
            return None
        elif plan == 'minute':
            cost = LUXURY_MINUTE * time

    if is_discounted == 'yes':
        cost *= DISCOUNT

    return cost


def main():
    URL = 'https://script.google.com/macros/s/AKfycbxReuav_j4FMBmT7i6ozd5E9oVjBOC7pKFyp5VlKjTRqmn7hjoeTDuzSayzMzTKKi2W/exec?action=get&service=calculatePrice&email=a.kurmazov%40innopolis.university&type={type}&plan={plan}&distance={distance}&planned_distance={planned_distance}&time={time}&planned_time={planned_time}&inno_discount={inno_discount}'
    
    result = []

    count = 0
    for _type in TYPES:
        for _plan in PLANS:
            for _distance in DISTANCES:
                for _planned_distance in PLANNED_DISTANCES:
                    for _time in TIMES:
                        for _planned_time in PLANNED_TIMES:
                            for _is_discounted in IS_DISCOUNTED:
                                count += 1

                                request_url = URL.format(**{
                                    'type': _type,
                                    'plan': _plan,
                                    'distance': _distance,
                                    'planned_distance': _planned_distance,
                                    'time': _time,
                                    'planned_time': _planned_time,
                                    'inno_discount': _is_discounted,
                                })

                                response = requests.get(request_url)
                                expected_price = calculate_ride(_type, _plan, _distance, _planned_distance, _time, _planned_time, _is_discounted)
                                price = json.loads(response.text)['price']
                                print(price, expected_price)

                                verdict = 'No error'
                                if None in (expected_price, price) and expected_price != price:
                                    verdict = 'Domain'
                                elif expected_price != price:
                                    verdict = 'Computational'

                                result.append(
                                    ','.join([
                                        str(count),  request_url, str(expected_price), str(price), verdict
                                    ])
                                )

    with open('result.csv', 'w') as out_file:
        out_file.write('\n'.join(result))


if __name__ == '__main__':
    main()
